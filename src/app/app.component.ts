import { Component } from '@angular/core';
import {UserService} from './user/user.service';
import {Http} from '@angular/http';
import { NavbarComponent } from './navbar/navbar.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app';
  posts ;

  constructor(service: UserService){
    this.posts = service.getUserPublicProfile() ;
    
  }
}
