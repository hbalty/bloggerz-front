import { Injectable } from '@angular/core';
import { FacebookService, InitParams, LoginOptions } from 'ngx-facebook';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class PermissionService {

    constructor(private fb: FacebookService, private httpclient: HttpClient) {
        let initParams: InitParams = {
            appId: '1128001770660612',
            xfbml: true,
            version: 'v2.12'
        };

        fb.init(initParams);
        
    }



    getUserGivenPermissions() {
        return this.fb.getLoginStatus().then(status => {
            let id = status.authResponse.userID;
            let access_token = status.authResponse.accessToken;
            return this.fb.api(
                id + '/permissions?access_token=' + access_token,
            );
        }).catch(error => {
            console.log('handled exception', error);
            return error;
        });
    }


    userSessionOn(){
        let user = localStorage.getItem('user'); 
        if (user !== null){
            return true;
        } else {
            return false;
        }
    }

    /**
     * Getting user public profile
    */
   getUserProfile(access_token) {
    return this.fb.api(
      '/me?fields=name,email&access_token=' + access_token,
      "get",
      response => {
          console.log(response) ;
      }
     );
    }



    getUserPermissions(permissions) {
        const options: LoginOptions = {
            scope: permissions,
            return_scopes: true,
            enable_profile_selector: true,
        };

        return this.fb.login(options).then(response => {
            return response;
        }).catch(error => {
            console.log('handled exception', error);
        });
    }

    getPageAccessToken(pageID) {
        return this.fb.getLoginStatus().then(status => {
          return this.fb.api(
            '/' + pageID + '?fields=access_token&access_token=' + status.authResponse.accessToken,
        ).then(state => {
          return state.access_token;
        });
      });

    }

    getFb() {
        return this.fb;
    }
}
