import { Injectable } from '@angular/core';
import { CanActivate, CanActivateChild, Router, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { AuthService, SocialUser, LoginProvider } from 'angular4-social-login';
import { OnInit } from '@angular/core';
import { LoginService } from '../login/login.service';
import { Observable } from 'rxjs';
import { PermissionService } from '../permission/permission.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable()
export class AuthGuardService implements CanActivate  {
  _loggedIn ;
  _user;

  constructor(private jwt: JwtHelperService,private permissionService: PermissionService ,private loginService: LoginService, private router: Router) {

  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      if (this.jwt.tokenGetter() === null){
        this.router.navigate(['login'])
        return false;
      } 
      if (!this.jwt.isTokenExpired(this.jwt.tokenGetter())) {
         return true;
      } else {
         this.router.navigate(['login']);
      }
      return true;

}


}
