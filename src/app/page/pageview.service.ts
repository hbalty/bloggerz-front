import {Http} from '@angular/http';
import {Injectable} from '@angular/core';
import { PermissionService } from '../permission/permission.service' ;




@Injectable()
export class PageviewService {
    private _pages ;
    private _realPages ;
    private fb;

    constructor(permissionService: PermissionService) {
        this.fb = permissionService.getFb() ;
    }



    getStat(since, until, metric ,pageId, access_token){
        return this.fb.api(
            '/'+pageId+'/insights?limit=100&period=day&metric='+ metric+'&since='+since+'&until='+until+'&access_token='+access_token,
            'GET',
            {},
            response => {
                console.log('getStat Response ',response)
                return response;
            }
        )
       
    }


    /**
     * Getting page public profile
    */
    getPageProfile(pageId, access_token) {
      return this.fb.api(
        '/' + pageId + '?fields=picture,fan_count,name,cover&access_token=' + access_token,
        'GET',
        {},
        response => {
            console.log(response) ;
        }
    );
    }

    /**
     * Getting page latest photos
    */
    getPageLatestPhotos(pageId, access_token) {
      return this.fb.api(
        '/' + pageId + '/feed?fields=id,link,picture,message,admin_creator&limit=30&access_token=' + access_token,
        'GET',
        {},
        response => {
            console.log(response) ;
        }
    );
    }

    /**
     * Getting page latest videos
    */
   getPageLatestVideos(pageId, access_token) {
    return this.fb.api(
      '/' + pageId + '/videos?fields=id,description,embed_html,thumbnails{uri}&access_token=' + access_token,
      'GET',
      {},
      response => {
          console.log('resp= ',response) ;
      }
  );
  }

    /**
     * Getting page latest videos
    */
   getPageLatestLinks(pageId, access_token) {
    return this.fb.api(
      '/' + pageId + '/posts?fields=id,link,description,shares&access_token=' + access_token,
      'GET',
      {},
      response => {
          console.log(response) ;
      }
  );
  }






    /*
    * Getting users pages
    * retunrs LocationAwarePromise
    */
    getPages(user_id, access_token) {
        return this.fb.api(
            '/' + user_id + '/accounts?fields=picture.type(large),cover,name,category,access_token&access_token=' + access_token,
            'GET',
            {},
            response => {
                console.log(response) ;
            }
        );
    }

    /**
     * getting page active users devided by the total number of likes
     */
    getPageIndicator(pageId, access_token) {
      return this.fb.api(
        pageId + '/insights?metric=page_fan_adds&access_token=' + access_token,
        'GET',
        {},
        response => {
            console.log(response) ;
        }
    );
    }


    getInsights(pageId, access_token, period: string , metrics: string = 'page_impressions,page_views_total,page_engaged_users') {
        //console.log(pageId +'/insights?metric=' + metrics + '&period=' + period + '&since='+'&access_token=' + access_token) ;
        return this.fb.api(
            pageId + '/insights?metric=' + metrics + '&period=' + period +'&access_token=' + access_token,
            'GET',
            {},
            response => {
                console.log(response) ;
            }
        );
    }

    getPageRoles(pageId, access_token){
        return this.fb.api(
            pageId+ '/roles?fields=picture,name,link,id,installed&access_token=' + access_token,
            'GET',
            {},
            response => {
                console.log(response)
            }
        )
    }


    /**
     * Posts
     */

     getPostInsights(postId,access_token){
         return this.fb.api(
             postId+'/insights?metric=post_consumptions,post_impressions,post_reactions_by_type_total,post_engaged_users&access_token='+access_token,
             'GET',
             {},
             response => {
                 
             }
         )
     }


     publishPost(PageId,post,token,date,message?){
        return this.fb.api(
            PageId+'/feed',
            'POST',
            {'link' : post.link,
            'published' : false,
            'message' : message,
            'scheduled_publish_time': date,
            'access_token' : token},
            response => {
                console.log('posted',response);
            }
        )
     }

}
