import {Component, Input} from '@angular/core' ;
import {PageviewService} from '../pageview.service';
import { PermissionService } from '../../permission/permission.service';
import { OnInit } from '@angular/core/src/metadata/lifecycle_hooks';
@Component({
    selector: 'app-maininsight',
    templateUrl: './main-insight.component.html',
})
export class MaininsightComponent implements OnInit {
    @Input() id: any ;
    @Input() metrics: any;
    @Input() period: any;
    data;

    constructor(private pageviewService: PageviewService, private permissionService: PermissionService) {
      this.data = { };
      
    }
    /**
     *
     */
    ngOnInit() {
      this.permissionService.getPageAccessToken(this.id).then(accessToken => {
        console.log(accessToken);
        this.pageviewService.getInsights(this.id, accessToken,
        this.period,
        this.metrics)
        .then(insights => {
        console.log('insights = ', insights) ;
        this.data = [insights.data] ;
      });
    });
  }

}
