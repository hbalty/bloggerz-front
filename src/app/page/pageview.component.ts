import {Component, EventEmitter, Output, Input, ViewChild} from '@angular/core' ;
import {PageviewService} from './pageview.service' ;
import {PermissionService} from '../permission/permission.service' ;
import { NgForm } from '@angular/forms/src/directives/ng_form';
import {OnInit} from '@angular/core' ;


@Component({
    'selector' : 'app-pages',
    'templateUrl' : './pageview.component.html',
    styleUrls: ['./pageview.component.css']
})



export class PageviewComponent implements OnInit {
    _pages;
    _realPages;
    _accessToken;
    @Input() filter;

    /**
     * @param page_service
     * @param permissionService
     */
    constructor(private page_service: PageviewService, private permissionService: PermissionService) {
      const today = new Date() ;
        console.log('today', today.getTime());
        const lastweek = new Date(today.getFullYear(), today.getMonth(), today.getDate() - 7 ).getTime();
        console.log("prociding to get user permissions ") ;

    }
    
    ngOnInit(){
        let permissions = JSON.parse(localStorage.getItem('user_permissions')) ;
         if (this.checkPermissionsGranted("manage_pages",permissions.data) && this.checkPermissionsGranted("pages_show_list",permissions.data)){
            console.log('true',permissions) ;
            this.permissionService.getUserPermissions('manage_pages,pages_show_list,read_insights').then(status => {
            this.binddata('day',
              'page_impressions,page_engaged_users,page_consumptions') ;
             }) ;
         }

    }

    getInsightsPermissions($event) {
        $event.preventDefault() ;
        this.permissionService.getUserGivenPermissions().then(permissions => {
            console.log(permissions) ;
             if (!this.checkPermissionsGranted("manage_pages",permissions.data) && !this.checkPermissionsGranted("pages_show_list",permissions.data)){
                 this.permissionService.getUserPermissions('manage_pages,pages_show_list,read_insights').then(status => {
                  this.binddata('day',
                  'page_impressions,page_engaged_users,page_consumptions') ;
                 }) ;
             } else {
                this.binddata('day','page_impressions,page_engaged_users,page_consumptions')
            }
         });
    }

    binddata(period, metrics) {
        const user = JSON.parse(localStorage.getItem('user'));
        //const loginStatus = this.permissionService.getFb().getLoginStatus() ;
        console.log(user.authToken);
        //loginStatus.then(status  => {
            this.page_service.getPages(user.id,
            user.authToken)
            .then(response => {
                console.log(response)
                this._pages = response.data ;
            }).catch(error => {
                console.log(error) ;
            }) ;

        //}) ;
    }

    getFilter() {
        return this.filter;
    }

    onSubmit() {
        console.log('submitted') ;
    }

    /**
     * Applying filter
     */
    notifyOnSubmit(f: NgForm): void {
      // console.log(f.value);
      this.filter = f.value;
      console.log(this.filter);
      let metrics = f.value.metrics.join(',');
      console.log('metrics = ', metrics) ;
      this.filter.since = f.value.since;
      this.filter.since = f.value.until;
      this.binddata(this.filter.period,metrics);

    }

    checkPermissionsGranted(permission: String, data : any[]) : Boolean {
        let exist = false ;
        console.log('check permissions granted', data);
        for (let element of data){
            console.log('element',element)
            if (element.permission === permission && element.status== "granted"){
                exist = true ;
            }
        }

        return exist ;
    }


}

