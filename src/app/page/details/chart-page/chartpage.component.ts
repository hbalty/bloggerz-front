import { Component, AfterViewInit, OnInit, AfterContentInit, ViewChild, ElementRef } from '@angular/core';
import { PageviewService } from '../../pageview.service';
import { FormGroup, FormControl } from '@angular/forms';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { PermissionService } from '../../../permission/permission.service';
import { NgbdDatepickerRange } from '../../../reusable/daterangepicker/datepicker-range';
import { NgbdModalOptions } from '../../../reusable/modal/modal-options';
import { NgbCalendar } from '@ng-bootstrap/ng-bootstrap/datepicker/ngb-calendar';
import { BrowserModule } from '@angular/platform-browser';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: '<chart-page></chart-page>',
  templateUrl: './chartpage.component.html',
  styleUrls: ['./chartpage.component.css']
})

export class ChartpageComponent implements AfterViewInit, OnInit {

  pageToken;
  private firstDataSetDates = {
    startDate: '03/01/2018',
    endDate: '03/30/2018'
  };

  private publicProfile;
  private pageRoles;


  private secondDataSetDates = {
    startDate: '2018-03-26',
    endDate: '2018-03-31'
  };

  private firstDataSet ; 
  private secondDataSet ;


  private updateChart;
  private id;

  // lineChart
  public lineChartData: Array<any> = [
  ];
  public lineChartLabels: Array<any> = [];
  public lineChartOptions: any = {
    responsive: true,
  };


  /**
   * Line chart colors specification
   */
  public lineChartColors: Array<any> = [
    { // grey
      backgroundColor: '#E3F2FD73',
      borderColor: '#90CAF9',
      pointBackgroundColor: '#2196F3',
      pointBorderColor: '#2196F3',
      pointHoverBackgroundColor: '#0D47A1',
      pointHoverBorderColor: '#0D47A1'
    },
    { // dark grey
      backgroundColor: 'rgba(255, 99, 132, 0.2)',
      borderColor: 'rgba(54, 162, 235, 0.2)',
      pointBackgroundColor: 'rgba(255, 206, 86, 0.2)',
      pointBorderColor: 'rgba(75, 192, 192, 0.2)',
      pointHoverBackgroundColor: 'rgba(153, 102, 255, 0.2)',
      pointHoverBorderColor: 'rgba(255, 159, 64, 0.2)'
    },
    { // grey
      backgroundColor: 'rgba(148,159,177,0.2)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];





  public lineChartLegend: boolean = true;
  public lineChartType: string = 'line';
  public Chart = {
    backgroundColor: this.lineChartColors,
    lineChartData: this.lineChartData,
    lineChartColors: this.lineChartColors,
    lineChartLabels: this.lineChartLabels,
    lineChartLegend: this.lineChartLegend,
    lineChartOptions: this.lineChartOptions,
    lineChartType: this.lineChartType
  }

  // constructor
  constructor(private permissionService: PermissionService,
    private route: ActivatedRoute,
    private pageViewService: PageviewService,
    private modalService: NgbModal
    ) {


    this.updateChart = false;

    this.route.paramMap
      .subscribe(params => {
        this.id = params.get('id');
      });






  }

 ngOnInit(){
     /**
     * Récupération du profil public 
     */

    this.permissionService.getPageAccessToken(this.id).then(accessToken => {
        this.pageViewService.getPageProfile(this.id,accessToken).then(publicProfile => {
          this.publicProfile = publicProfile;
          console.log(this.publicProfile);
        })

        this.pageViewService.getPageRoles(this.id,accessToken).then(pageRoles => {
           this.pageRoles = pageRoles;
           console.log('admins',this.pageRoles)
        })
    })

 }


  ngAfterViewInit() {
    this.updateChart = false;
    this.updateChartValues();
  }

  updateChartValues() {

    this.permissionService.getPageAccessToken(this.id).then(accessToken => {
      this.pageToken = accessToken;

        this.Chart.lineChartLabels = [];
        return this.pageViewService.getStat(this.firstDataSetDates.startDate, this.firstDataSetDates.endDate,
          'page_engaged_users',
          this.id,
          this.pageToken).then((ressources) => {
            this.prepareChartLabels(ressources.data[0].values);
            this.updateChart = true;
            this.Chart.lineChartData.push(this.prepareChartData(ressources));
            this.firstDataSet = this.prepareChartData(ressources);
          });

    }).catch(error => {
      console.log('is it');
      throw error;
    })
  }


  updateSecondChartValues() {
    this.permissionService.getPageAccessToken(this.id).then(accessToken => {
      this.pageToken = accessToken;

        this.pageViewService.getStat(this.secondDataSetDates.startDate, this.secondDataSetDates.endDate,
          'page_engaged_users',
          this.id,
          this.pageToken).then((ressources) => {
            this.updateChart = true;
            this.Chart.lineChartData.push(this.prepareChartData(ressources));
          });

    }).catch(error => {
      console.log('is it');
      throw error;
    })
  }

  addValuesToChart(startDate: string, endDate: string) {
    console.log('add values to chart');
    this.pageViewService.getStat(startDate, endDate, 'page_engaged_users', this.id, this.pageToken).then(data => {
      this.updateChart = true;
      this.Chart.lineChartData.push(this.prepareChartData(data))
    })
    console.log('line chart data', this.Chart.lineChartData);
  }


  // adding values to the chart
  prepareChartData(ressources) {
    let firstDataSet: number[] = [];
    let firstDataLabels: number[] = [];
    for (let input of ressources.data[0].values) {
      firstDataSet.push(input.value);
    }
    return { data: firstDataSet, label: ressources.data[0].title};
  }



  // events
  public chartClicked(e: any): void {
    console.log(e);
  }

  public chartHovered(e: any): void {
    console.log(e);
  }


  prepareChartLabels(values: number[]) {
    for (let i = 1; i < values.length; i++) {
      this.Chart.lineChartLabels.push('day ' + i);
    }
  }


  // Recieve emitters data and updates the chart 
  getDate($event) {
    this.updateChart = false;
    if ($event.id === 1) {
      if (this.Chart.lineChartData.length == 2){
        this.Chart.lineChartData.pop();
        this.Chart.lineChartData.pop();
      }
      this.firstDataSetDates.startDate = $event.date.fromDate.month + '/' + $event.date.fromDate.day + '/' + $event.date.fromDate.year;
      this.firstDataSetDates.endDate = $event.date.toDate.month + '/' + $event.date.toDate.day + '/' + $event.date.toDate.year;
      this.updateChartValues();
    } else if ($event.id === 2) {
      if (this.Chart.lineChartData.length == 2){
        this.Chart.lineChartData.pop();
        this.Chart.lineChartData.pop();
      }
      this.secondDataSetDates.startDate = $event.date.fromDate.month + '/' + $event.date.fromDate.day + '/' + $event.date.fromDate.year;
      this.secondDataSetDates.endDate = $event.date.toDate.month + '/' + $event.date.toDate.day + '/' + $event.date.toDate.year;
      this.updateSecondChartValues();
    }


  }

}
