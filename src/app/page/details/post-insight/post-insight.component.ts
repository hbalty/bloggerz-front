import {Component, Input, OnInit} from '@angular/core' ;
import {PageviewService} from '../../pageview.service';
import { PermissionService } from '../../../permission/permission.service';
import { ActivatedRoute } from '@angular/router';


@Component({
    selector: 'app-postinsight',
    templateUrl: './post-insight.component.html',
})
export class PostinsightComponent implements OnInit {
    id: any ;
    @Input() postId;
    data;
    bind;

    constructor(private route: ActivatedRoute ,private pageviewService: PageviewService, private permissionService: PermissionService) {
      this.data = { };
      this.route.paramMap
      .subscribe(params => {
        this.id = params.get('id');
      });
      this.bind=false;
      
    }

    /**
     *
     */
    
    ngOnInit() {
      try{
        this.permissionService.getPageAccessToken(this.id).then(accessToken => {
          this.pageviewService.getPostInsights(this.postId,accessToken)
          .then(insights => {
           this.data = insights.data;
           this.bind = true;
           console.log(this.data);
        });
      });
      }catch(error){
        console.log(error)
      }
  }

}
