import { Component } from '@angular/core' ;
import { RouterModule, Routes, Router } from '@angular/router';
import { AuthService, SocialUser } from 'angular4-social-login';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoginService } from '../login/login.service';

@Component({
    "selector": '<app-navbar></app-navbar>',
    "templateUrl" : './navbar.component.html' ,
    styleUrls: ['../../assets/css/navbar.css']
})

export class NavbarComponent {
    isLoggedIn;
    constructor(private authService: AuthService,private router: Router, private jwt: JwtHelperService, private loginService: LoginService) {
        this.isLoggedIn = !this.jwt.isTokenExpired();
    }


    logout() {
        console.log('logging out')
        this.loginService.signOut();
        this.router.navigate(['**']);
    }


}
