import {Input, Injectable} from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {PermissionService} from '../permission/permission.service';
import { JwtWrapperService } from '../guards/jwt-wrapper.service';
import { AppSettings } from '../reusable/app-settings';
import { JwtHelperService } from '@auth0/angular-jwt';

//http://127.0.0.1:8000




@Injectable()
export class ReviewOffersService {

  constructor(private jwt:  JwtHelperService ,private http: HttpClient, private jwtWrapper: JwtWrapperService , private permissionService: PermissionService){

  }

    getActiveOffers(){

      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Bearer ' + this.jwt.tokenGetter()
        })
      };
      return this.http.get(AppSettings.API_ENDPOINT + 'review-offers', httpOptions);
    }


    addReviewOffer(post) {
      console.log(this.jwtWrapper.getToken());
      const httpOptions = {
        headers: new HttpHeaders({
          'Content-Type':  'application/json',
          'Authorization': 'Bearer ' + this.jwtWrapper.getToken()
        })
      };
        post.owner = '06868';

        console.log(post);
        return this.http.post(AppSettings.API_ENDPOINT + 'review-offer/add', post, httpOptions)

    }

    postReviewOffer(post) {
      const headers = new HttpHeaders({
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Bearer ' + this.jwtWrapper.getToken()});

      const options = { headers : headers};

      let body     = new URLSearchParams();

      // Appending all the post to the request body
      body = post;


      return this.http.post(AppSettings.API_ENDPOINT + 'review-offer/add', post, options);



    }


}
