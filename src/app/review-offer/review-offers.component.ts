import {Component} from '@angular/core';
import {ReviewOffersService} from './review-offers.service';
import { HttpClient } from '@angular/common/http';


@Component({
  selector: '<review-offers></review-offers>',
  templateUrl: './review-component.html'
})

export class ReviewOffersComponent{
  constructor(private http: HttpClient, private reviewOffersService : ReviewOffersService){
    

      this.reviewOffersService.getActiveOffers().subscribe(data => {
        console.log(data) ;
      });
  }





}
