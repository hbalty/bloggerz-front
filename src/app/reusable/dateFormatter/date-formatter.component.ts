import { NgbDateParserFormatter } from "@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-parser-formatter";
import { NgbDateStruct } from "@ng-bootstrap/ng-bootstrap/datepicker/ngb-date-struct";
import { isNumber } from "util";
import { padNumber } from "@ng-bootstrap/ng-bootstrap/util/util";



export class customNgBFormatter implements NgbDateParserFormatter {

  constructor() {
    console.log("intialized");
  }


    format(date: NgbDateStruct) {
      return date ?
      `${date.year}-${isNumber(date.month) ? padNumber(date.month) : ''}-${isNumber(date.day) ? padNumber(date.day) : ''}T00:00:00+01:00` :
      '';
    }

    parse(date: string) {
      let struct : NgbDateStruct;
      return struct;
    }

}
