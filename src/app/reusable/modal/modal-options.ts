import {Component, ViewEncapsulation, Output, Input, EventEmitter} from '@angular/core';
import {NgbModal, ModalDismissReasons} from '@ng-bootstrap/ng-bootstrap';
import { NgbdDatepickerRange } from '../daterangepicker/datepicker-range';

@Component({
  selector: 'ngbd-modal-options',
  templateUrl: './modal-options.html',
  encapsulation: ViewEncapsulation.None,
  styles: [`
    .dark-modal .modal-content {
      background-color: #292b2c;
      color: white;
    }
    .dark-modal .close {
      color: white;
    }
    .light-blue-backdrop {
      background-color: #5cb3fd;
    }
  `]
})
export class NgbdModalOptions {
  closeResult: string;
  fromDate: string = '-';
  toDate: string = '-';
  @Input() id;
  @Input() content ;

  @Output() sendDateRange: EventEmitter<any> = new EventEmitter();
    sendDate(date) {
        console.log('emetting from modal')
        this.sendDateRange.emit({id: this.id, date: date});
    }


  constructor(private modalService: NgbModal) {

  }

  openBackDropCustomClass(content) {
    this.modalService.open(content, {backdropClass: 'light-blue-backdrop'});
  }

  openWindowCustomClass(content) {
    this.modalService.open(content, { windowClass: 'dark-modal' });
  }

  openSm(content) {
    this.modalService.open(content, { size: 'sm' });
  }

  openLg(content) {
    this.modalService.open(content, { size: 'lg' });
  }

  openVerticallyCentered(content) {
    this.modalService.open(content, { centered: true });
  }

  getDate($event) {
    this.fromDate = $event.fromDate.month + '/' + $event.fromDate.day + '/' + $event.fromDate.year;
    this.toDate = $event.toDate.month + '/' + $event.toDate.day + '/' + $event.toDate.year;
    console.log('id = ', this.id) ;
    this.sendDate($event);
  }







}


